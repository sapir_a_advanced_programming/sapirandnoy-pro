#pragma once
#include <iostream>
#include <map>
#include <vector>
#include "User.h"

using namespace std;

class Game
{
private:
//	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
//	DataBase& _db;
	map<string, int> _results;
	int _currTurnAnswers;

	bool inserGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();

public:
	//Game(const vector<User*>& vec, int, DataBse&);

};