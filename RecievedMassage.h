 #pragma once
#include <iostream>
#include <string>
#include <vector>

#include "User.h"

class RecievedMessage
{
private:
	SOCKET _sock;
	User* _user;
	int _msgCode;
	vector<string> _values;

public:
	RecievedMessage(SOCKET sock, int code);
	RecievedMessage(SOCKET sock, int code, vector<string> vec);
	SOCKET getSock();
	int getMessageCode();
	vector<string> getValues();
	User* getUser();
	void setUser(User* user);

};