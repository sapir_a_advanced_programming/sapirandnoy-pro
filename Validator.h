#pragma once
#include <string>
#include <cstring>

#define PASS_SIZE 4

using namespace std;

class Validator 
{
public:
	static bool isPasswordValid(string pass);
	static bool isUsernameValid(string uname);

};
