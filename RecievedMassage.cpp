#include "RecievedMassage.h"

RecievedMessage::RecievedMessage(SOCKET sock, int code)
{
	_sock = sock;
	_msgCode = code;
}

RecievedMessage::RecievedMessage(SOCKET sock, int code, vector<string> vec)
{
	_sock = sock;
	_msgCode = code;
	_values = vec;
}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}

int RecievedMessage::getMessageCode()
{
	return _msgCode;
}

vector<string> RecievedMessage::getValues()
{
	return _values;
}

User * RecievedMessage::getUser()
{
	return _user;
}

void RecievedMessage::setUser(User * user)
{
	this->_user = user;
}
