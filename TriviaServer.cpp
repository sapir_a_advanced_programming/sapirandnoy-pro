#include "TriviaServer.h"

#define END_CONNECTION 299


static const unsigned short PORT = 7896;
static const unsigned int IFACE = 0;

TriviaServer::TriviaServer() //c'tor
{
	//add calling db constractor
	_sock = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_sock == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

}

TriviaServer::~TriviaServer()//d'tor
{
	while (!_queRecievedMessage.empty()) // deleting the recieved messages queue
	{
		_queRecievedMessage.pop();
	}
	_connectedUsers.clear(); // deleting the list of the users from the map
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{//closing the socket
		::closesocket(_sock);
	}
	catch (...) {}
}

/*
handling with the messages and getting clients
*/
void TriviaServer::serve()
{
	bindAndListen();
	std::thread tHandleMessage(&TriviaServer::handleRecievedMessage, this);
	tHandleMessage.detach();

	while (true)
	{
		accept();
	}
}

/*
start binding to socket and listening.
*/
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sockAddress = { 0 };
	sockAddress.sin_port = htons(PORT);
	sockAddress.sin_family = AF_INET;
	sockAddress.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_sock, (struct sockaddr*)&sockAddress, sizeof(sockAddress)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	TRACE("binded");

	if (::listen(_sock, SOMAXCONN) == SOCKET_ERROR)
	{

		throw std::exception(__FUNCTION__ " - listen");
	}
	TRACE("listening...");

}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_sock, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}
	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread clientsThread(&TriviaServer::clientHandler, this, client_socket);
	clientsThread.detach();
}

void TriviaServer::clientHandler(SOCKET sock)
{
	RecievedMessage* msg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(sock);

		while (msgCode != 0 && msgCode != END_CONNECTION)
		{
			msg = buildRecieveMessage(sock, msgCode);
			addRecievedMessage(msg);

			msgCode = Helper::getMessageTypeCode(sock);
		}

		msg = buildRecieveMessage(sock, END_CONNECTION);
		addRecievedMessage(msg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << sock << ", what=" << e.what() << std::endl;
		msg = buildRecieveMessage(sock, END_CONNECTION);
		addRecievedMessage(msg);
	}
	closesocket(sock);
}

void TriviaServer::safeDeleteUser(RecievedMessage * recMsg)
{
	try
	{
		this->handleSignout(recMsg);
		::closesocket(recMsg->getSock()); //closing socket
	}
	catch(...)
	{

	}

}

User * TriviaServer::handleSignin(RecievedMessage * recMsg)
{
	string msg;
	User * currUser = recMsg->getUser();
	int nameSize = Helper::getIntPartFromSocket(recMsg->getSock(), 2);
	string username = Helper::getStringPartFromSocket(recMsg->getSock(), nameSize);
	int passSize = Helper::getIntPartFromSocket(recMsg->getSock(), 2);
	string password = Helper::getStringPartFromSocket(recMsg->getSock(), passSize);
	//bool matching = DataBase::isUserAndPassMatch(name, pass);
	bool matching = false;
	for (int i = 0; i < _isSigned.size(); i++)
	{
		if (_isSigned.at(i)->getUsername() == username)
		{
			matching = true;
		}
	}
	if (matching)
	{
		if (currUser != nullptr)
		{
			//send fail msg
			msg = "1022";
			currUser->send(msg);

		}
		else
		{
			_connectedUsers.insert(pair<SOCKET, User*>(recMsg->getSock(), currUser)); // adding the user to the connected users list
			//send success to client
			msg = "1020";
			currUser->send(msg);
			return currUser;
		}
	}
	else
	{
		//send fail msg
		msg = "1021";
		currUser->send(msg);
	}
	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage * recMsg)
{
	int nameSize = Helper::getIntPartFromSocket(recMsg->getSock(), 2);
	string username = Helper::getStringPartFromSocket(recMsg->getSock(), nameSize);
	int passSize = Helper::getIntPartFromSocket(recMsg->getSock(), 2);
	string password = Helper::getStringPartFromSocket(recMsg->getSock(), passSize);
	int mailSize = Helper::getIntPartFromSocket(recMsg->getSock(), 2);
	string mail = Helper::getStringPartFromSocket(recMsg->getSock(), passSize);
	string msg;

	try
	{
		if (Validator::isPasswordValid(password))
		{
			if (Validator::isUsernameValid(username))
			{
				/*if(DataBase::isUserExists(name))
				{
					send fail msg - user already exists
					msg = "1042";
				}
				else
				{
					DataBase::addNewUser(name,pass,mail);
					send success msg
					msg = "1040"

				}*/
			}
			else
			{
				//send fail msg - not good username
				msg = "1043";
			}
		}
		else
		{
			//send fail msg - not good password	
			msg = "1041";
		}

		recMsg->getUser()->send(msg);
	}
	catch(...)
	{
		msg = "1044";
		recMsg->getUser()->send(msg);
	}
	
	
	return false;
}

void TriviaServer::handleSignout(RecievedMessage * recMsg)
{
	if (recMsg->getUser() != nullptr)
	{
		_connectedUsers.erase(recMsg->getSock());
		handleCloseRoom(recMsg);
		handleLeaveRoom(recMsg);
		handleLeaveGame(recMsg);
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage * recMsg)
{
	if (recMsg->getUser()->leaveGame())
	{
		::free(recMsg->getUser()->getGame()); // deleting the room
	}
}

void TriviaServer::handleStartGame(RecievedMessage * recMsg)
{
	try
	{

	}
	catch (...) {}
}

void TriviaServer::handleRecievedMessage()
{	
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	RecievedMessage* currMessage = _queRecievedMessage.front();
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMassage);

			// in case the queue is empty.
			if (_queRecievedMessage.empty())
			{
				continue;
			}

			_queRecievedMessage.pop();
			lck.unlock();

			currMessage->setUser(getUserBySocket(currMessage->getSock()));
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();

			if (msgCode == END_CONNECTION || msgCode == 0 || msgCode > 300 || msgCode < 200)
			{
				safeDeleteUser(currMessage);
			}
			else
			{
				//switch (msgCode)
				//{
				//case 200:
				//	//signin
				//	User * newUser = handleSignin(currMessage);
				//	_connectedUsers.insert(pair<SOCKET, User*>(clientSock, newUser));
				//		break;
				//case 201:
				//	//signout
				//	handleSignout(currMessage);
				//	break;
				//case 203:
				//	bool success = handleSignup(currMessage);
				//	//signup
				//	break;
				//case 205:
				//	//rooms list
				//	handleGetRooms(currMessage);
				//	break;
				//case 207:
				//	//users in room
				//	handleGetUsersInRoom(currMessage);
				//	break;
				//case 209:
				//	//join room
				//	handleJoinRoom(currMessage);
				//	break;
				//case 211:
				//	//leaving room
				//	break;
				//case 213:
				//	//create room
				//	break;
				//case 215:
				//	//close room
				//	break;
				//case 217:
				//	//start game
				//	break;
				//case 219:
				//	//user's answer
				//	break;
				//case 222:
				//	//leaving game
				//	break;
				//case 223:
				//	//best score
				//	break;
				//case 225:
				//	//personal status
				//	break;
				//case 299:
				//	//exit game
				//	break;
				//}
			}

			delete currMessage;
		}
		catch (...)
		{
			safeDeleteUser(currMessage);
		}
	}
}

void TriviaServer::addRecievedMessage(RecievedMessage * recMsg)
{
	//lock mutex
	_queRecievedMessage.push(recMsg);
	//unlock mutex
}

RecievedMessage * TriviaServer::buildRecieveMessage(SOCKET sock, int msgCode)
{  
	vector<string> values;

	if (msgCode == 200)
	{
		int nameSize = Helper::getIntPartFromSocket(sock, 2);
		string username = Helper::getStringPartFromSocket(sock, nameSize);
		int passSize = Helper::getIntPartFromSocket(sock, 2);
		string password = Helper::getStringPartFromSocket(sock, passSize);
		values.push_back(username);
		values.push_back(password);
	}
	else if (msgCode == 201 || msgCode == 205 || msgCode == 211 || msgCode == 215 || msgCode == 217)
	{
		//Do nothing
	}
	else if (msgCode == 203)
	{
		int nameSize = Helper::getIntPartFromSocket(sock, 2);
		string username = Helper::getStringPartFromSocket(sock, nameSize);
		int passSize = Helper::getIntPartFromSocket(sock, 2);
		string password = Helper::getStringPartFromSocket(sock, passSize);
		int mailSize = Helper::getIntPartFromSocket(sock, 2);
		string mail = Helper::getStringPartFromSocket(sock, passSize);
		values.push_back(username);
		values.push_back(password);
	}
	else if (msgCode == 207 || msgCode == 209 || msgCode == 222 || msgCode == 223 || msgCode == 225 || msgCode == 299)
	{
		string roomId = Helper::getStringPartFromSocket(sock, 4);
		values.push_back(roomId);
	}
	else if (msgCode == 213)
	{
		int roomNameSize = Helper::getIntPartFromSocket(sock, 2);
		string roomName = Helper::getStringPartFromSocket(sock, roomNameSize);
		string playersNum = Helper::getStringPartFromSocket(sock, 1);
		string questionNum = Helper::getStringPartFromSocket(sock, 2);
		string questionTime = Helper::getStringPartFromSocket(sock, 2);
	}
	else if (msgCode == 219)
	{
		string answerNumber = Helper::getStringPartFromSocket(sock, 1);
		string answerTime = Helper::getStringPartFromSocket(sock, 2);
	}

	RecievedMessage * msg = new RecievedMessage(sock, msgCode, values);
	return msg;
}

User * TriviaServer::getUserByName(string name)
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		User* currUser = it->second;
		if (currUser->getUsername() == name)
		{
			return currUser;
		}
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET sock)
{
	return _connectedUsers.at(sock);
}

Room * TriviaServer::getRoomById(int id)
{
	return _roomsList.at(id);
}
